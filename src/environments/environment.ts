// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlRestrictiveList: 'https://backdigitalbc.wahc.site/client/findByParams',
  //authServiceEndpoint: 'https://0ad4f944-8b24-4264-a077-6d3a7f0464a6.mock.pstmn.io/auth',
  authServiceEndpoint:'http://179.12.226.34:8080/bff/user/login',
  clientListEndpoint: 'https://5b4e4664-98e5-470a-aa7c-39d2a462a014.mock.pstmn.io/client-list'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
