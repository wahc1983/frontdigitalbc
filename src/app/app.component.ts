import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { TokenStorageService } from './services/token-storage/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit{
  title = 'frontDigitalBC';
  private roles: string[] = [];
  isLoggedIn = false;
  showAdmin = false;
  showReader = false;
  userName?: string;

  closeResult: string = '';
  constructor(public loaderService: LoaderService, 
              private tokenStorageService: TokenStorageService) {}

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.userName = user.userName;
    }    
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }

}
