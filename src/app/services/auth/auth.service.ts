import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TokenStorageService } from '../../services/token-storage/token-storage.service';
import { JwtHelperService } from "@auth0/angular-jwt";
import { AuthResponse } from '../../models/authResponse';
import { shareReplay } from 'rxjs/operators';

const authEndpoint = environment.authServiceEndpoint;
const jwtHelper = new JwtHelperService ();
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private token: TokenStorageService,
              private http: HttpClient) { }

  login(usuario: string, password: string): Observable<AuthResponse> {
    return this.http.post<AuthResponse>(authEndpoint, {
      usuario,
      password
    }, httpOptions).pipe(shareReplay(1));
  }  

  public isAuthenticated(): boolean {
    const token = this.token.getToken();
    // Check whether the token is expired and return
    // true or false
    if (token != null) {
      return true;
      //return jwtHelper.isTokenExpired(token);
    } else {
      return false
    }
  } 

}
