import { Injectable } from '@angular/core';

const CL_KEY = 'client-list';
@Injectable({
  providedIn: 'root'
})
export class CreateClientService {

  constructor() { }

  createClient(data: any): void {
    let clients = this.getClientsInfo();
    clients[data.documentNumber] = data;
    this.saveClientList(clients);
    console.log(clients)
  }

  getClientsInfo(): any{

    const clients = window.sessionStorage.getItem(CL_KEY);
    if (clients) {
      return JSON.parse(clients);
    }
    return {};    
  }

  saveClientList(data: any): void{
    window.sessionStorage.removeItem(CL_KEY);
    window.sessionStorage.setItem(CL_KEY, JSON.stringify(data));
  }  

}
