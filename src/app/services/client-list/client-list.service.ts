import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { shareReplay } from 'rxjs/operators';

const clEndpoint = environment.clientListEndpoint;

@Injectable({
  providedIn: 'root'
})
export class ClientListService {

  constructor(private http: HttpClient) { }

  getClientList(): Observable<any>{
    return this.http.get<any>(clEndpoint).pipe(shareReplay(1));
  }

}
