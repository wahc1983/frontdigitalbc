import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from "rxjs";
import { ClientValidationResponse } from '../../models/clientValidationResponse';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class RestrictiveListsService {

  private endPoint: string = environment.urlRestrictiveList;

  respondsService = [{pass:true, code:200, message:'El cliente pasó la validacion'},
                     {pass:false, code:201, message:'El cliente no pasó la validacion'}]

  constructor(private http: HttpClient) { }

  validateClientInList(inParams:any): Observable<ClientValidationResponse>{

    const params = new HttpParams()
    .set('document_type', inParams.documentType)
    .set('document_number', inParams.documentNumber)
    .set('name', inParams.name)
    .set('lastname', inParams.lastName)
    .set('document_issue_date', inParams.documentIssueDate)
    .set('birth_date', inParams.birthDate)
    .set('email', inParams.email)
    .set('cellphone', inParams.cellphone);

    return this.http.get<ClientValidationResponse>(this.endPoint, {params});
  }
}
