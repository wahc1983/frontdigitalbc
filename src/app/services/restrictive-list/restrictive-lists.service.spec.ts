import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { RestrictiveListsService } from './restrictive-lists.service';
import { environment } from 'src/environments/environment';
import { ClientValidationResponse } from '../../models/clientValidationResponse';

describe('RestrictiveListsService', () => {
  let service: RestrictiveListsService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      // providers: [RestrictiveListsService],
    });
    service = TestBed.inject(RestrictiveListsService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('RestrictiveListService should make a successful request', (done) => {
    const testUrl = environment.urlRestrictiveList;

    const clientData  = {
      // data que se le va a enviar al servicio, lo que traemos del form
      birthDate: '2021-09-01',
      cellphone: '+573182345678',
      documentIssueDate: '2021-09-02',
      documentNumber: 1234567890,
      documentType: 'CC',
      email: 'mail@mail.com',
      lastName: 'apellido',
      name: 'name'
    };
    const expectedResponse: ClientValidationResponse = {
      // el json que te responde el backend con la data de un caso correcto
      date: '',
      client_reported: '',
      message: '',
      status: ''
    };
    service
      .validateClientInList(clientData)
      .subscribe((data: any) => {
        expect(data).toEqual(expectedResponse);
      });
    const httpMock = httpTestingController.expectOne({
      method: 'GET'
      // url: `${testUrl}/?document_type=CC&document_number=14136394&name=william&lastname=huertas&document_issue_date=2021-09-23&birth_date=2021-09-02&email=ares1015@hotmail.com&cellphone=+573173799077`,
    });
    expect(httpMock.request.method).toBe('GET');
    httpMock.flush(expectedResponse);
    httpTestingController.verify();
    done();
  });
});