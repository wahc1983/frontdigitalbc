export interface IDocumentType {
    description: string;
    acronym: string;
}