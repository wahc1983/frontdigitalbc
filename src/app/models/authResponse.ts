export interface AuthResponse {
    jwt: string;
    status: string;
    message: string;
    client_reported: string;
    date: string;
}