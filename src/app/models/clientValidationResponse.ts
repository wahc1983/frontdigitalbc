export interface ClientValidationResponse {
    date: string;
    client_reported: string;
    message: string;
    status: string;
}