import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientValidationComponent } from './components/client-validation/client-validation.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { ClientListComponent } from './components/client-list/client-list.component';
import { ClientDetailComponent } from './components/client-detail/client-detail.component';
import { CreateClientComponent } from './components/create-client/create-client.component';

const routes: Routes = [
  {path: 'client-validation' , component: ClientValidationComponent, canActivate:[AuthGuardService]},
  {path: 'create-client' , component: CreateClientComponent, canActivate:[AuthGuardService]},
  {path: 'client-detail/:id', component: ClientDetailComponent, canActivate:[AuthGuardService]},
  {path: 'login', component:LoginComponent},
  {path: '', component: ClientListComponent,  canActivate:[AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
