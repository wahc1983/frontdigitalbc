import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { faUserCircle, faPlusCircle, faArrowLeft } from '@fortawesome/free-solid-svg-icons';

const CL_KEY = 'client-list';
@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.sass']
})
export class ClientDetailComponent implements OnInit {
  clientDetails:any;
  clientId:any = '';
  faUserCircle = faUserCircle;
  faArrowLeft = faArrowLeft;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getClientId();
    this.clientDetails = this.getClientsInfo()[this.clientId];
    console.log(this.clientDetails[this.clientId])
  }

  getClientId(): void{
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.clientId = params.get('id')
    });    
  }

  getClientsInfo(): any{

    const clients = window.sessionStorage.getItem(CL_KEY);
    if (clients) {
      return JSON.parse(clients);
    }
    return {};    
  }

}
