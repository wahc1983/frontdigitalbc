import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormsModule, FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { RestrictiveListsService } from 'src/app/services/restrictive-list/restrictive-lists.service';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import { CreateClientService } from 'src/app/services/create-client/create-client.service';
import { NgxUiLoaderService, Loader, SPINNER, POSITION, PB_DIRECTION } from 'ngx-ui-loader';
import { LoaderService } from 'src/app/services/loader/loader.service';
import { IDocumentType } from 'src/app/models/documentType';
import { faUserCircle, faPlusCircle, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [`
    .modal-body {
      background-color: #fff;
      color: #E52023;
    }
    .close {
      color: #E52023;
    }
  `],  
  styleUrls: ['./create-client.component.sass']
})
export class CreateClientComponent implements OnInit {

  title: string = 'Por favor ingrese los datos del cliente:';
  messageModal:string = '';
  faArrowLeft = faArrowLeft;
  maxLength = '15';
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [CountryISO.Colombia, CountryISO.UnitedStates]; 
  formClientValidation!: FormGroup;
  documentTypes: IDocumentType[] = [
    { description: 'Cédula de ciudadanía', acronym: 'CC' },
    { description: 'Cédula de extranjería', acronym: 'CE' },
    { description: 'Número de identificación personal', acronym: 'NIP' },
    { description: 'Número de identificación tributaria', acronym: 'NIT' },
    { description: 'Tarjeta de identidad', acronym: 'TI' },
    { description: 'Pasaporte', acronym: 'PAP' },
  ];

  constructor(private fb: FormBuilder,
              private modalService: NgbModal,
              private restrictiveListsService: RestrictiveListsService,
              private ngxLoaderService: NgxUiLoaderService,
              private createClientService:CreateClientService,
              public router: Router,
              public loaderService: LoaderService) {
    this.loaderService.config.logoUrl = '../assets/images/scotiabank-colpatria-symbol-red.svg';
    this.loaderService.config.logoSize = 150;
    this.loaderService.config.text = 'Procesando...';
    this.loaderService.config.hasProgressBar = false;
    this.loaderService.config.fgsType = SPINNER.foldingCube;
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.formClientValidation = this.fb.group({
      documentType: ['', Validators.required],
      documentNumber: ['', [Validators.required, Validators.max(9999999999), Validators.pattern('^[0-9]*$')]],
      documentIssueDate: ['', [Validators.required]],
      name: ['', [Validators.required,  Validators.maxLength(12), Validators.pattern('^[a-zA-Z \-\']+')]],
      lastName: ['', [Validators.required,  Validators.maxLength(12), Validators.pattern('^[a-zA-Z \-\']+')]],
      birthDate: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email, Validators.maxLength(45)]],
      cellphone: ['', [Validators.required]]
    });
  }

  get f(): any { return this.formClientValidation.controls; }

  createClient(modalId: any): void {
    if (this.formClientValidation.invalid) {
      return;
    }    
    this.ngxLoaderService.startLoader('loader-request');
    const formC = this.formClientValidation.value;
    formC.birthDate = this.formatDate(formC.birthDate);
    formC.documentIssueDate = this.formatDate(formC.documentIssueDate);
    formC.cellphone = this.formClientValidation.controls.cellphone.value.e164Number;
    
    setTimeout(() => {
      this.validateClient(formC, modalId);
    }, 2000);
  }

  validateClient(form: any, modalId: any): void {
      const resp = this.restrictiveListsService.validateClientInList(form).subscribe(
        item => {
          console.log(item)
          if(item.client_reported == 'NO'){
            this.createClientService.createClient(form);
            this.ngxLoaderService.stopLoader('loader-request');
            this.router.navigate([''])
          } else {
            this.messageModal = item.message;
            this.modalService.open(modalId, { size: 'sm' });
            this.ngxLoaderService.stopLoader('loader-request');
            return;
          }          
        },
        error => {
          console.error(error.message);
          this.messageModal = error.message;
          this.modalService.open(modalId, { size: 'sm' });
          return;
        }
      );   

  }

  openModal(modalId: any): void{
    this.modalService.open(modalId);
  }

  formatDate(date: string): string {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  } 

}
