import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroup, ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';
import { ClientValidationComponent } from './client-validation.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ClientValidationComponent', () => {
  let component: ClientValidationComponent;
  let fixture: ComponentFixture<ClientValidationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientValidationComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('form invalid when empty', () => {
  //   expect(component.formClientValidation.valid).toBeFalsy();
  // });
});
