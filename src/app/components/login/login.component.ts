import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { TokenStorageService } from '../../services/token-storage/token-storage.service';
import { FormBuilder, FormsModule, FormControl, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  messageModal:string = '';
  formLogin!: FormGroup;
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  constructor(private authService: AuthService,
              private modalService: NgbModal, 
              public router: Router,
              private tokenStorage: TokenStorageService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      //this.roles = this.tokenStorage.getUser().roles;
    }    
  }

  initForm(): void {
    this.formLogin = this.fb.group({
      password: ['', Validators.required],
      userName: ['', [Validators.required, Validators.min(6)]],
    });
  }

  get f(): any { return this.formLogin.controls; }

  loginApp(modalId: any): void {
    console.log('login')
    if (this.formLogin.invalid) {
      return;
    }    
    const { userName, password } = this.formLogin.value;

    this.authService.login(userName, password).subscribe(
      data => {
        this.tokenStorage.saveToken(data.jwt);
        this.tokenStorage.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        //this.roles = this.tokenStorage.getUser().roles;
        this.router.navigate(['']).then(() => {
          window.location.reload();
        });
      },
      err => {
        console.error(err);
        this.messageModal = 'No se pudo validar el usuario, verifique las credenciales e intentelo de nuevo';
        this.modalService.open(modalId, { size: 'sm' });        
        this.isLoginFailed = true;
      }
    );
/*
    this.tokenStorage.saveToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9obiBEb2UiLCJyb2xlcyI6ImFkbWluIn0.I0FeWSggnijmNxjLdycWA2_kcnRFf5DkFZ57fg-VDaI");
    this.tokenStorage.saveUser({"name": "John Doe", "roles": "admin"});
    this.isLoginFailed = false;
    this.isLoggedIn = true;
    this.roles = this.tokenStorage.getUser().roles;
    this.router.navigate(['']).then(() => {
      window.location.reload();
    });   
*/
  }

  openModal(modalId: any): void{
    this.modalService.open(modalId);
  }

}
