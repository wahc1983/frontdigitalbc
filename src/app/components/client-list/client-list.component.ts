import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/services/token-storage/token-storage.service';
import { faUserCircle, faPlusCircle, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { ClientListService } from 'src/app/services/client-list/client-list.service';
import { Router } from '@angular/router';

const CL_KEY = 'client-list';
@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.sass']
})
export class ClientListComponent implements OnInit {
  username: string = '';
  clients:any = [];
  faUserCircle = faUserCircle;
  faPlusCircle = faPlusCircle;
  faSignOutAlt = faSignOutAlt;
  constructor(private clService:ClientListService, private token: TokenStorageService) { }
  
  ngOnInit(): void {
    this.username = this.token.getUser().user_name;
    let clientsSaved = this.getClientsInfo();
    console.log(clientsSaved)
    if(Object.keys(clientsSaved).length === 0){
      this.clService.getClientList().subscribe(
        data => {
          for(let i in data){
            this.clients.push(data[i]);
          }
          this.saveClientList(data)
        },
        err => {
          console.error(err);
        }
      );      
    } else {
      for(let i in clientsSaved){
        this.clients.push(clientsSaved[i]);
      }      
    }

  }

  saveClientList(data: any): void{
    window.sessionStorage.removeItem(CL_KEY);
    window.sessionStorage.setItem(CL_KEY, JSON.stringify(data));
  }

  getClientsInfo(): any{
    const clients = window.sessionStorage.getItem(CL_KEY);
    if (clients) {
      return JSON.parse(clients);
    }
    return {};    
  }  

  singOut(): void {
    this.token.signOut();
    this.reloadPage(); 
  }

  reloadPage(): void {
    window.location.reload();
  }  

}
