import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ClientValidationComponent } from './components/client-validation/client-validation.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import {
  NgxUiLoaderModule,
  NgxUiLoaderConfig,
  SPINNER,
  POSITION,
  PB_DIRECTION,
  NgxUiLoaderRouterModule,
  NgxUiLoaderHttpModule  
} from "ngx-ui-loader";
import { LoaderService } from 'src/app/services/loader/loader.service';
import { LoginComponent } from './components/login/login.component';
import { authInterceptorProviders } from './helpers/auth.interceptor';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { ClientListComponent } from './components/client-list/client-list.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ClientDetailComponent } from './components/client-detail/client-detail.component';
import { CreateClientComponent } from './components/create-client/create-client.component';

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsColor: "red",
  pbColor: 'red',
  fgsColor: 'red',
  bgsPosition: POSITION.bottomCenter,
  bgsSize: 40,
  bgsType: SPINNER.rectangleBounce, 
  fgsType: SPINNER.threeStrings, 
  pbDirection: PB_DIRECTION.leftToRight, 
  pbThickness: 5, 
  fastFadeOut: true,  
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ClientValidationComponent,
    LoginComponent,
    ClientListComponent,
    ClientDetailComponent,
    CreateClientComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,  
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatFormFieldModule,
    HttpClientModule,
    NgxIntlTelInputModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    NgxUiLoaderRouterModule,
    NgxUiLoaderHttpModule,
    FontAwesomeModule    
  ],
  providers: [LoaderService, authInterceptorProviders, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
